# GitLab Docs Shell

A command line interface to work locally on [GitLab Docs](https://gitlab.com/gitlab-org/gitlab-docs).

## What it does

Introduces an easier and faster way to work on GitLab Documentation through
`docs [options]` commands.

With `docs` commands, you can:

- Clone all repos (GitLab Docs, GitLab, Omnibus, Runner, and Charts) or set up the local paths to your repos (GDK is supported).
- Check, install, and update GitLab Docs dependencies.
- Pull all repos in one command and housekeep to keep them clean.
- Compile GitLab Docs and live preview. <!-- To-do: preview on mobile. -->
- Run all linters.

## Who's this for

- New contributors to GitLab Docs - the easiest way to set it up to work locally.
- Current contributors - the easiest way to update, compile, recompile, and lint GitLab Docs, and to keep the dependencies up-to-date.

## How to use this project

[Install](#installation) Docs Shell to optimize your workflow for updating,
compiling, linting, and previewing GitLab Docs locally.

## Requirements

Docs Shell is optimized for [iTerm2 + ZSH + oh-my-zsh](https://medium.com/ayuth/iterm2-zsh-oh-my-zsh-the-most-power-full-of-terminal-on-macos-bdb2823fb04c) on macOS Catalina.

We assume you're familiar with using Git through the command line and GitLab.

### Docs Shell dependency

ZSH shell is a hard-requirement for Docs Shell. ZSH comes installed on macOS Catalina.

To check which shell you're using, open your terminal and run:

```shell
echo $SHELL
```

If it returns `/bin/zsh`, you're good to go. If not, check if you have it installed:

```shell
zsh --version
```

If it returns a version number, such as `zsh 5.7.1`, set ZSH as default shell:

```shell
chsh -s /bin/zsh
```

If ZSH is not present in your computer, see [how to install](https://github.com/ohmyzsh/ohmyzsh/wiki/Installing-ZSH) it according to your OS.

**Note:** if you'd like Docs Shell to install ZSH, iTerm2, and oh-my-zsh, after the step 3 of the [installation process](#installation), run `docs pre-install`.

### GitLab Docs dependencies

During the installation, all GitLab Docs' dependencies will be installed (if not present). Mind that:

- NodeJS: Docs Shell will prompt you to install it with Homebrew.
  - Alternative method: [Install Node with a version manager (nvm)](https://github.com/nvm-sh/nvm#installation-and-update).
  - Alternative method: [Install Node directly](https://nodejs.org/en/download/).
- Ruby: Docs Shell will prompt you to install Ruby with [RVM](http://rvm.io/). If you don't use or don't want to use `rvm` to manage Rubies, make sure to install the current [Ruby version](https://gitlab.com/gitlab-org/gitlab-docs/-/blob/master/.ruby-version) of GitLab Docs.

## Installation

1. [Fork](https://docs.gitlab.com/ee/gitlab-basics/start-using-git.html#fork)
Docs Shell into your namespace and [clone](https://docs.gitlab.com/ee/gitlab-basics/start-using-git.html#clone-a-repository)
onto your computer into a folder where we can clone
[GitLab Docs and all projects it pulls from](https://gitlab.com/gitlab-org/gitlab-docs#projects-we-pull-from):
GitLab, Omnibus, Runner, and Charts.
1. `cd` into the `docs-shell` directory and run `. ./docs.sh && access` to
grant terminal access to Docs Shell. Follow the prompt steps.
1. Run `docs hello` to test it.
   1. (Optional): run `docs pre-install` to install ZSH, iTerm2, and oh-my-zsh.
1. Run `docs install` to check all the dependencies and clone all repos or to set up your existing repos' paths (you'll be prompted to clone or not).
1. You're ready to [get started](#get-started).

See [**installation details**](docs/installation.md) for further info.

## Usage

Check workflow examples below. See also [all commands available](#available-commands).

### Get started

1. Check if the `docs` command is working properly: `docs hello`.
1. Pull all repos: `docs pull`.
1. Compile the Docs site and start live preview: `docs compile live`. Docs Shell will open a browser window or tab to preview the docs site. Give it a few seconds to load.
1. Open any GitLab document (`gitlab/doc/file.md`) and make a change.
1. Run `docs recompile`. Refresh your browser to see the changes.

### Linting

1. Pull all repos: `docs pull`.
1. Compile the Docs site: `docs compile`.
1. Run content linters (Vale and markdownlint) on the GitLab repo: `docs lint -c gitlab`.
1. Run only Vale on the GitLab repo: `docs lint -c gitlab vale`.
1. Run Nanoc linters: `docs lint -n`.
1. Lint only anchors: `docs lint -n anchors`.

### Checking dependencies

1. Pull GitLab Docs: `docs pull docs`.
1. Run: `docs dep`.
1. To update all the dependencies: `docs dep --update`.

### Navigate between repos

1. Open the terminal and run `docs hello`. This will bring you to the Docs Shell directory.
1. Run `docs go_gitlab` to open the GitLab repo. Also available: `go_docs`, `go_shell`, `go_omnibus`, `go_runner`, `go_charts`.

### Interrupt

To interrupt a running job, press <kbd>control</kbd> + <kbd>C</kbd> on your
keyboard.

Run `docs kill` to kill `nanoc live` if you have it running on the background.

## Available commands

```bash
# Test or trigger `docs`:
docs hello

# Docs Shell usage:
docs --help

# Installation:
docs install # option to clone all repos or set up custom paths
docs pre-install # option to check ZSH, install iTerm2, and oh-my-zsh

# Change paths:
docs paths # Change custom paths
docs paths --reset # reset to default (cloned) paths

# Check dependencies:
docs dep
# Update and check dependencies:
docs dep --update

# Lint:
docs lint # lints both content and links
# - Lint options:
docs lint --content | -c # lint content
docs lint --nanoc   | -n # lint gitlab-docs

# Lint content options:
docs lint -c [repo]
# - Example:
docs lint -c gitlab
# - Lint content repo options:
docs lint -c [repo] [linter]
# - Examples:
docs lint -c gitlab markdown
docs lint -c gitlab vale

# Lint gitlab-docs options:
docs lint -n
# Options:
docs lint -n anchors
docs lint -n links

# Compile:
docs compile
docs compile live

# Preview GitLab Docs:
docs live

# Recompile:
docs recompile
docs recompile lint # recompile and lint content and nanoc
docs recompile lint live # recompile, lint content and nanoc, preview docs

# Pull repos
docs pull # checkout master, pull all repos (housekeeping)
docs pull [repo] [repo] ... [repo] # checkout master, pull
# - Examples:
docs pull gitlab # pull GitLab
docs pull gitlab runner # Pull GitLab and GitLab Runner
docs pull docs # Pull GitLab Docs

# Similar to `docs pull` commands, more granular
docs update
docs update --skip # updates all repos except GitLab
docs update -h # (housekeeping)
docs update -h -s # (housekeeping + stash EE)
docs update -a # (all doc/ repos quick + gitlab-docs)
docs update -h -a #(housekeeping all)

# Nanoc direct commands
docs nanoc compile
docs nanoc live

# Kill nanoc live
docs kill

# Navigation
docs go_gitlab
docs go_docs
docs go_runner
docs go_omnibus
docs go_charts
docs go_shell
```

### Limitations

- Docs Shell is written and tested on masOS Catalina. If you have a different OS, please test it and open an issue or a merge request in this project if you find any problems.
- Previewing docs partially (for example, compile the site with only `gitlab` docs, not including Omnibus, Runner, and Charts) has not been tested, so it can cause unforeseen errors.

## Info

For references, see [info](docs/info.md).

## License

Copyright (c) 2020 [Marcia Ramos](https://gitlab.com/marcia). See [LICENSE](LICENSE) for
further details.

Contributions are very welcome.
