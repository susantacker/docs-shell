# GitLab Docs Shell documentation

## Start to work

1. Update all repost, compile docs, live preview:

   ```bash
   docs update -h -a && docs compile live
   ```

1. Add content, recompile:

   ```bash
   docs recompile
   ```

<!-- 
### Update only FOSS, Omnibus, Runner, Charts

Use case: you started working on `GitLab` files and want to update the rest of
the repos pulled into `gitlab-docs`. For example, to be able to run the docs
linters with the latest changes, you'll need all repos updated to avoid false
positive results.

- **Pull all repos** quickly: `sh update-docs-quick.sh`.
- **Pull all repos and clean them up** with housekeeping (gc and prune): `sh update-docs-housekeeping.sh`.

### Compile and preview only `gitlab-docs`

Use case: you already have the repos updated and want to preview `gitlab-docs` locally.

- **Pull `gitlab-docs`, compile, lint, live preview:** `sh docs-compile.sh`.

### Update FOSS, Omnibus, Runner, Charts, and compile and preview `gitlab-docs`

Use case: you're working on changes to `GitLab` files and you want to update all the other repos, and also build and preview `gitlab-docs`.

  - **Pull all repos, compile, lint, and run live preview:** `sh update-compile-quick.sh`.

### Update GitLab, FOSS, Omnibus, Runner, Charts, and compile and preview `gitlab-docs`

Use case: before starting to work on GitLab files, you want to update all repos and also build and preview `gitlab-docs`.

- **Warning:** ensure your GitLab repo doesn't have any unsaved changes.
- **Pull all repos, compile, lint, and run live preview:** `sh update-compile-all.sh`.

### Recompile `gitlab-docs`

Use case: you are already working on `GitLab` files and you want to recompile
`gitlab-docs` and run the linters for broken links and anchors.

- **Recompile docs:** `sh recompile.sh`.
- **Recompile and lint docs:** `sh recompile-lint.sh`.
 -->

## Tips

### Find, retrieve, and open local directory paths in the terminal

On macOS, to quickly get the path for a given directory, open it in your
terminal, or to retrive the path your terminal is currently on:

- To find a directory path: open Finder, drag your directory, drop into the terminal. Press <kbd>return</kbd> to open it in the terminal.
- If you have a directory opened in a terminal window, run `pwd` to retrieve
the path.
