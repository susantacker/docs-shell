# Installation

The file tree Docs Shell considers **default** is:

```text
.
├── main-dir
│   ├── docs-shell
│   ├── gitlab-docs
│   ├── gitlab
│   ├── omnibus-gitlab
│   ├── gitlab-runner
│   └── charts
```

`main-dir` can be any directory.

In case you don't have your repos organized like this, you may face issues
with Docs Shell. Please report any eventual errors so we can fix them.

When you run `docs install`, the first thing Docs Shell will ask you is if you already have all repos cloned onto your computer:

```shell
Do you have all repos (GitLab [or GDK], Omnibus, Runner, Charts, Docs) cloned already (y/n)?
```

If you have all repos already, type "y" and press enter to set up your
existing folders as [paths](#paths) to the projects GitLab pulls from.
Otherwise, see [clone](#clone).

## Paths

Docs Shell will then prompt you to inform the paths to each repo, starting from:

```shell
Set custom path for your GitLab Shell repo (y/n)?
```

If you have the default file structure, you don't have to add each path;
type "n" and press enter and Docs Shell will assume the default structure.
Otherwise, type "y" and press enter for every repo you have in a different
directory and you'll be prompted to add its custom path.

For example, suppose you have the GitLab repo somewhere and it isn't GDK. When Docs Shell asks you:

```shell
Set custom path for your GitLab repo (y/n)?
```

Type "y" and press enter. You'll be asked:

```shell
Is it GDK (y/n)?
```

Type "n" and press enter.

```shell
Enter the path to your GitLab repo:
```

Add:

```shell
/Users/username/path/to/gitlab
```

Docs Shell will ask you the same thing for each repo.

TIP: to find the full path to a given folder, open it in a terminal and run
`pwd` and your shell will give you its path. Then you can copy it and paste it
wherever you want. Or, even easier, open Finder and the terminal side-by-side
and drag the folder from Finder to the terminal.

### GDK

If you use GitLab Development Kit, you can add it as custom path and work directly there. When Docs Shell asks:

```shell
Set custom path for your GitLab repo (y/n)?
```

Type "y" and press enter. Shell will prompt you:

```shell
Is it GDK (y/n)?
```

Type "y" and press enter again. Then:

```shell
Enter the path to your GitLab (GDK) repo:
```

And add the full path to your repo and press enter:

```shell
/Users/username/path/to/gdk
```

Docs Shell will consider your `gdk/gilab/doc` folder to build GitLab Docs on
the docs site, so you can edit docs directly there while you work on GDK. It
will watch docs live preview on port `3005` instead of conflicting with GDK's
`3000`.

### Reset paths

If you need to change the paths, you can run `docs install` again, or simply `docs paths`.

If you want to reset to Docs Shell default paths, run `docs paths --reset`.

## Clone

If you're starting fresh, Docs Shell can clone all repositories for you.

Make sure to clone this project into the folder where you want to clone all
the other repos needed into.

Docs Shell will ask you if you're a GitLab Team member to choose the correct
namespace. If you are a GitLabber, Docs Shell will assume `gitlab-org` as
namespace. Otherwise, it will ask you what's the namespace you forked your
projects into.

So, if you aren't a GitLabber, fork all repos before installing Docs Shell,
making sure you've forked all of them into the same namespace. Once done, run
`docs install` to begin.

You'll be prompted:

```shell
Do you have all repos (GitLab [or GDK], Omnibus, Runner, Charts, Docs) cloned already (y/n)?
```

Type "n" and press enter to start the cloning process. Docs Shell will try to
find an SSH connection to GitLab.com via `ssh -T git@gitlab.com`. If found,
you'll be led to clone through SSH. Otherwise, you'll be prompted to clone
through HTTPS.

If you have the projects forked into different namespaces, manually clone them
into the same directory on your computer then run `docs install`. Answer "y"
when Docs Shell asks "Do you have all repos (GitLab [or GDK], Omnibus, Runner,
Charts, Docs) cloned already (y/n)?", and from there you can set your
[custom paths](#paths).

## Compilation

Once the paths are set, Docs Shell will compile the site and open a browser
tab or window on the docs site. Give it a few seconds and it will display the
docs site locally.
