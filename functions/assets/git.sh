## Git

test_git() {
  echo 'functions/assets/git.sh ok'
}

git_status() {
  echo `tput bold` "$ git status" `tput sgr0`; git status
}

git_pull() {
  echo `tput bold` "$ git pull" `tput sgr0`; git pull
}

git_gc() {
  echo `tput bold` "$ git gc" `tput sgr0`; git gc
}

git_prune() {
  echo `tput bold` "$ git prune" `tput sgr0`; git prune
}

git_master() {
  echo `tput bold` "$ git checkout master" `tput sgr0`; git checkout master
}

git_stash() {
  echo `tput bold` "$ git stash" `tput sgr0`; git stash
}

git_config_rebase() {
  echo `tput bold` "$ git config pull.rebase false" `tput sgr0` ; git config pull.rebase false
}

## Git combinations

git_update_quick(){
  git_status; git_pull
}

git_update_house() {
  git_status; git_master; git_pull; git_gc; git_prune
}

git_checkout_master(){
  git_status; git_master;
}

git_stash_checkout_master(){
  git_status; git_stash; git_master;
}
