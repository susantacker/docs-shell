# Tests all files

test(){

  if [[ $1 == 'files' ]]; then {
    echo 'Running file source tests...'
    tput setaf 2; echo '1. alerts:'; tput sgr0; test_alerts
    tput setaf 2; echo '2. git:'; tput sgr0; test_git
    tput setaf 2; echo '3. nav:'; tput sgr0; test_navigation
    tput setaf 2; echo '4. vars:'; tput sgr0; test_variables
    tput setaf 2; echo '5. compile:'; tput sgr0; test_compile
    tput setaf 2; echo '6. update:'; tput sgr0; test_update
    tput setaf 2; echo '7. lint content:'; tput sgr0; test_lint_content
    tput setaf 2; echo '8. lint gitlab-docs:'; tput sgr0; test_lint_gitlab-docs
    tput setaf 2; echo '9. test-install:'; tput sgr0; test_install
    tput setaf 2; echo '10. test-install-ruby:'; tput sgr0; test_install_ruby
    tput setaf 2; echo '11. test-install-dependencies:'; tput sgr0; test_install_dep
    tput setaf 2; echo '12. test-update-dependencies:'; tput sgr0; test_update_dep
    tput setaf 2; echo '13. test-install-clone:'; tput sgr0; test_install_clone
    tput setaf 2; echo '14. test-install-linters:'; tput sgr0; test_install_linters
    tput setaf 2; echo '15. test-install-symlinks:'; tput sgr0; test_symlinks
    tput setaf 2; echo '16. paths:'; tput sgr0; test_paths
    tput setaf 2; echo '17. pull:'; tput sgr0; test_pull
    tput setaf 2; echo '9. test-pre-install:'; tput sgr0; test_preinstall
    }
    ifsuccess
  fi
}
