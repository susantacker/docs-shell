### Variables ###

test_variables() {
  echo 'functions/assets/variables.sh ok'
}

## Emojis

UNICORN='\360\237\246\204'
PAW='\360\237\220\276'
BLUSH='\360\237\230\212'
CHECK='\363\276\255\212'
SWEATSMILE='\360\237\230\205'
GEAR='\342\232\231'
WRENCH='\360\237\224\247'
WAVE='\360\237\221\213'
SAD='\360\237\230\236'
TADA='\360\237\216\211'

## Time and date

NOW=$(date +"%Y-%m-%d at %T")
