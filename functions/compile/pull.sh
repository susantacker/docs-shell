# Test file (docs test files)
test_pull() {
  echo 'compile/pull.sh ok'
}

# ---------------------------------------------------------------- #

# Pull all repos
# Usage: docs pull (pull and housekeep all repos); docs pull [repo] [repo] ... [repo]
# Examples: docs pull gitlab; docs pull gitlab runner; docs pull omnibus gitlab shell; docs pull docs
pull() {
  if [[ $@ ]] ; then
    for arg in "$@" ; do
      go_$arg
      if [[ $? != 0 ]]; then
        echo "Possible variations: \n $ docs pull \n $ docs pull [repo] [repo] ... [repo]"
        echo "   Options [repo]: gitlab, runner, omnibus, charts, docs, shell"
        return
      else
        tput setaf 5 ; echo "Pulling $arg" ; tput sgr0
        git_status
        git_master
        git_config_rebase
        git_pull
      fi
    done
  else
    update -h -a
  fi
  cd $DSHELL
}
