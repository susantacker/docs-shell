# Test files (docs test files)
test_lint_gitlab-docs() {
  echo 'functions/lint/gitlab-docs.sh ok'
}

# ---------------------------------------------------------------- #

# Lint GitLab Docs #

docs_lint(){
  go_docs
  echo `tput setaf 5`"Linting for broken links and anchors..." `tput sgr0`
  echo `tput bold` "$ bundle exec nanoc check internal_links" `tput sgr0`; bundle exec nanoc check internal_links
  ifsuccess
  echo `tput bold` "$ bundle exec nanoc check internal_anchors" `tput sgr0`; bundle exec nanoc check internal_anchors
  ifsuccess
}

docs_lint_anchors() {
  go_docs
  echo `tput setaf 5`"Linting for broken anchors..." `tput sgr0`
  echo `tput bold` "$ bundle exec nanoc check internal_anchors" `tput sgr0`; bundle exec nanoc check internal_anchors
  ifsuccess
}

docs_lint_links() {
  go_docs
  echo `tput setaf 5`"Linting for broken links..." `tput sgr0`
  echo `tput bold` "$ bundle exec nanoc check internal_links" `tput sgr0`; bundle exec nanoc check internal_links
  ifsuccess
}

docs_lint_while_live(){
  echo `tput setaf 3`"Running docs live preview on the background..." `tput sgr0`
  docs_live & docs_lint
  echo `tput setaf 3`"Refresh a page in the browser to bring docs live to the foreground." `tput sgr0`
}

docs_all() {
  go_docs
  docs_compile
  docs_lint_while_live
}

docs_recompile_lint(){
  go_docs
  docs_recompile
  docs_lint
}

docs_recompile_lint_live(){
  go_docs
  docs_recompile
  docs_lint_while_live
}
