# Script for ZSH

# Test file (docs test files)
test_install_linters() {
  echo 'install/linters.sh ok'
}

# ---------------------------------------------------------------- #

# markdownlint

depcheck_mdlint () {
  echo `tput setaf 5`"Checking markdownlint..." `tput sgr0`
  echo `tput bold` "$ markdownlint --version" `tput sgr0`; markdownlint --version
}

check_mdlint() {
  if ! depcheck_mdlint ; then
    echo `tput setaf 5`"Not installed yet. Installing..." `tput sgr0`
    echo `tput bold` "$ npm install -g markdownlint-cli" `tput sgr0`; npm install -g markdownlint-cli
    if [[ $? != 0 ]] ; then
      echo $?
      tput setaf 3 ; read "REPLY?Is the error "`tput sgr0`"'Missing write access to /usr/local/lib/node_modules'? "`tput setaf 3`"If so, can I run "`tput sgr0`"'$ sudo chown -R \$USER /usr/local/lib/node_modules' "`tput setaf 3`"to try to fix it (y/n)?" ; tput sgr0
        if [[ $REPLY =~ ^[Yy]$ ]] ; then
          echo `tput bold` "$ sudo chown -R \$USER /usr/local/lib/node_modules" `tput sgr0`; sudo chown -R $USER /usr/local/lib/node_modules
        else
          echo "Ok, I'll leave that to you."
        fi
    else
      tput setaf 2; printf "markdownlint installed successfully. `tput sgr0` (${NOW}) \n"
    fi
  fi
}

# ---------------------------------------------------------------- #

# Vale

depcheck_vale () {
  echo `tput setaf 5`"Checking Vale..." `tput sgr0`
  echo `tput bold` "$ vale --version" `tput sgr0`; vale --version
}

check_vale() {
  if ! depcheck_vale ; then
    echo `tput setaf 5`"Not installed yet. Installing..." `tput sgr0`
    echo `tput bold` "$ brew install vale" `tput sgr0`; brew install vale
    if [[ $? == 0 ]] ; then
      tput setaf 2; printf "Vale installed successfully. `tput sgr0` (${NOW}) \n"
    fi
  fi
}
