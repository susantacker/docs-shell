# Script for ZSH

# Test file (docs test files)
test_install_ruby() {
  echo 'install/ruby.sh ok'
}

# ---------------------------------------------------------------- #

# Load current gitlab-docs Ruby (from `master`)

use_ruby(){
  rvm use $(set_rv) >/dev/null
}

# ---------------------------------------------------------------- #

# Check gitlab-docs and system Rubies

# Curl .ruby-version from GitLab Docs master branch
set_rv() {
  curl -s https://gitlab.com/gitlab-org/gitlab-docs/-/raw/master/.ruby-version > .ruby-version
}

# Cat Ruby version
docsrv() {
  set_rv
  cat .ruby-version
}

# Cat system's Ruby
localrv() {
  cd $DSHELL
  rvm current
}

# ---------------------------------------------------------------- #

# Check RVM

# Install RVM
rvm-install() {
  echo `tput bold` "$ curl -sSL https://get.rvm.io | bash -s stable" `tput sgr0` ; curl -sSL https://get.rvm.io | bash -s stable
}

# Install gitlab-docs Ruby with RVM
rvm-install-ruby() {
  DOCSRUBY=$(docsrv)
  echo `tput bold` "$ rvm install $DOCSRUBY" `tput sgr0` ; rvm install $DOCSRUBY
  echo `tput bold` "$ rvm use $DOCSRUBY" `tput sgr0` ; rvm use $DOCSRUBY
}

# rvm all set (check/install rvm, rvm install Ruby)
rvm-set() {
  if ! depcheck_rvm ; then
    rvm-install
  fi
  if ! depcheck_ruby ; then
    rvm-install-ruby
  fi
  tput setaf 2; printf "Rubies ready! `tput sgr0` (${NOW}) \n"
}

depcheck_rvm() {
  echo 'Checking RVM...'
  echo `tput bold` '$ rvm --version' `tput sgr0` ; rvm --version
}

check_rvm() {
  if ! depcheck_rvm ; then
    tput setaf 3
    read  "REPLY?Recommended: Would you like to install RVM (y/n)?"
    tput sgr0
    if [[ $REPLY =~ ^[Yy]$ ]] ; then
      rvm-set
    else
      echo "Okay, let's try without it."
    fi
  fi
} 

# ---------------------------------------------------------------- #

# Check Ruby

# Check if Ruby is installed and use gitlab-docs Ruby
depcheck_ruby() {
  echo 'Checking Ruby version...'
  docsrv
  echo `tput bold` "$ rvm use $(docsrv)" `tput sgr0` ; rvm use $(docsrv)
  echo  `tput bold` '$ ruby --version' `tput sgr0` ; ruby --version
}

check_ruby() {
  if ! depcheck_ruby ; then
    error_sound ; tput setaf 3 ; read "REPLY?Required: you need Ruby $(docsrv). Would you like to install Ruby through RVM (y/n)?"
    tput sgr0
    if [[ $REPLY =~ ^[Yy]$ ]] ; then
      rvm-install-ruby
    else
      echo `tput setaf 1`"Failed, you need Ruby $(docsrv)." `tput sgr0` 'See https://www.ruby-lang.org/en/ for reference.\nAborting.'
      false
    fi
  else
    echo `tput setaf 5`"Checking if your local Ruby version meets the requirement..." `tput sgr0`
    check_compare_rubies
  fi
}

check_compare_rubies() {
  RVM=ruby-$(docsrv)
  echo "GitLab Docs (remote master) version is `tput bold`ruby-$(docsrv)`tput sgr0`."
  echo "Local version is `tput bold`$(localrv)`tput sgr0`."

  if [[ $(localrv) = $RVM ]] ; then
    echo `tput setaf 2`'Rubies match.'`tput sgr0`
  else
    echo `tput setaf 1`'Failed, rubies do not match.'`tput sgr0`
    error_sound ; tput setaf 3 ; read "REPLY?Recommended: Would you like to install Ruby version $(docsrv) (y/n)?"
    tput sgr0
    if [[ $REPLY =~ ^[Yy]$ ]] ; then
      rvm-install-ruby
    else
      error_sound ; echo `tput setaf 1`"Failed, you need Ruby $(docsrv)." `tput sgr0` 'See https://www.ruby-lang.org/en/ for reference. \n Aborting.'
      false
    fi
  fi
}
