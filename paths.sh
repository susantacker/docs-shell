# Test files (docs test files)
test_paths() {
  echo 'paths.sh ok'
}

# ---------------------------------------------------------------- #

# Read the folder this repo was cloned into and set it as $LOCAL
# To check, run echo $LOCAL

_loc(){
  cd .. && pwd
}

CURRENT=$PWD

# ---------------------------------------------------------------- #

# Read current paths #

# Local
path_to_local() {
  awk 'NR==2' .paths
}

# GL
path_to_gl() {
  awk 'NR==4' .paths
}

# Shell
path_to_shell() {
  awk 'NR==6' .paths
}

# Docs
path_to_docs() {
  awk 'NR==8' .paths
}

# GitLab
path_to_gitlab() {
  awk 'NR==10' .paths
}

# Omnibus
path_to_omn() {
  awk 'NR==12' .paths
}

# Runner
path_to_run() {
  awk 'NR==14' .paths
}

# Charts
path_to_char() {
  awk 'NR==16' .paths
}

user_variables(){
  echo 'LOCAL=$(path_to_local)'
  echo 'GL=$(path_to_gl)'
  echo 'GITLAB=$(path_to_gitlab)'
  echo 'OMN=$(path_to_omn)'
  echo 'RUN=$(path_to_run)'
  echo 'CHAR=$(path_to_char)'
  echo 'DOC=$(path_to_docs)'
  echo 'DSHELL=$(path_to_shell)'
} > functions/assets/user-variables.sh

# ---------------------------------------------------------------- #

# Set custom paths `docs paths`, `docs paths --reset` (reset all to default), docs paths --gdk

paths() {
  if [ -e functions/assets/user-variables.sh ] ; then
    rm functions/assets/user-variables.sh
  fi
  if [[ $1 == "--reset" ]]; then
    default_paths
  else
    if [ -e .paths ] ; then
      echo "Removing current paths..." ; rm .paths
    fi
    # Default
    LOCAL=$(_loc)
    GL=$LOCAL/
    printf "DEFAULT \$LOCAL:\n$LOCAL\n" > .paths
    printf "DEFAULT \$GL:\n$GL\n" >> .paths

    # GitLab Shell
    tput setaf 3
    ok_sound ; read "REPLY?Set custom path for your GitLab Shell repo (y/n)?"
    tput sgr0
    if [[ $REPLY =~ ^[Yy]$ ]] ; then
      echo "Enter the path to your GitLab Shell repo:"
      read CUSTOM_DSHELL
      DSHELL=$CUSTOM_DSHELL
      printf "CUSTOM \$DSHELL:\n$DSHELL\n" >> .paths
    else
      DSHELL=$GL'docs-shell'
      printf "DEFAULT \$DSHELL:\n$DSHELL\n" >> .paths
    fi

    # GitLab Docs
    tput setaf 3
    ok_sound ; read "REPLY?Set custom path for your GitLab Docs repo (y/n)?"
    tput sgr0
    if [[ $REPLY =~ ^[Yy]$ ]] ; then
      echo "Enter the path to your GitLab Docs repo:"
      read CUSTOM_DOC
      DOC=$CUSTOM_DOC
      printf "CUSTOM \$DOC:\n$DOC\n" >> .paths
    else
      DOC=$GL'gitlab-docs'
      printf "DEFAULT \$DOC:\n$DOC\n" >> .paths
    fi

    # GitLab
    ok_sound ; tput setaf 3 ; read "REPLY?Set custom path for your GitLab repo (y/n)?" ; tput sgr0
      if [[ $REPLY =~ ^[Yy]$ ]] ; then
        tput setaf 3 ; read "REPLY?Is it GDK (y/n)?" ; tput sgr0
        if [[ $REPLY =~ ^[Yy]$ ]] ; then
          ok_sound ; echo "Enter the path to your GitLab (GDK) repo:"
          read CUSTOM_GITLAB
          GITLAB=$CUSTOM_GITLAB
          printf "CUSTOM \$GITLAB (GDK):\n$GITLAB\n" >> .paths
          symlink_gdk
          DGDK='0'
        else
          ok_sound ; echo "Enter the path to your GitLab repo:"
          read CUSTOM_GITLAB
          GITLAB=$CUSTOM_GITLAB
          printf "CUSTOM \$GITLAB:\n$GITLAB\n" >> .paths
          symlink_gitlab
        fi
    else
      GITLAB=$GL'gitlab'
      printf "DEFAULT \$GITLAB:\n$GITLAB\n" >> .paths
      symlink_gitlab
    fi

    # Omnibus
    tput setaf 3
    ok_sound ; read "REPLY?Set custom path for your Omnibus GitLab repo (y/n)?"
    tput sgr0
    if [[ $REPLY =~ ^[Yy]$ ]] ; then
      ok_sound ; echo "Enter the path to your Omnibus GitLab repo:"
      read CUSTOM_OMN
      OMN=$CUSTOM_OMN
      printf "CUSTOM \$OMN:\n$OMN\n" >> .paths
      symlink_omnibus
    else
      OMN=$GL'omnibus-gitlab'
      printf "DEFAULT \$OMN:\n$OMN\n" >> .paths
      symlink_omnibus
    fi

    # Runner
    tput setaf 3
    ok_sound ; read "REPLY?Set custom path for your GitLab Runner repo (y/n)?"
    tput sgr0
    if [[ $REPLY =~ ^[Yy]$ ]] ; then
      ok_sound ; echo "Enter the path to your GitLab Runner repo:"
      read CUSTOM_RUN
      RUN=$CUSTOM_RUN
      printf "CUSTOM \$RUN:\n$RUN\n" >> .paths
      symlink_runner
    else
      RUN=$GL'gitlab-runner'
      printf "DEFAULT \$RUN:\n$RUN\n" >> .paths
      symlink_runner
    fi

    # Charts
    tput setaf 3
    ok_sound ; read "REPLY?Set custom path for your GitLab Charts repo (y/n)?"
    tput sgr0
    if [[ $REPLY =~ ^[Yy]$ ]] ; then
      ok_sound ; echo "Enter the path to your GitLab Charts repo:"
      read CUSTOM_CHAR
      CHAR=$CUSTOM_CHAR
      printf "CUSTOM \$CHAR:\n$CHAR\n" >> .paths
      symlink_charts
    else
      CHAR=$GL'charts'
      printf "DEFAULT \$CHAR:\n$CHAR\n" >> .paths
      symlink_charts
    fi

    symlink_check_all
    tput setaf 3 ; echo `tput bold`"Check the result:"`tput sgr0` ; cat .paths
  fi
  user_variables
  if [[ $DGDK = '0' ]] ; then
    echo "PORT='3005'" >> functions/assets/user-variables.sh
  else
    echo "PORT='3000'" >> functions/assets/user-variables.sh
  fi
  # Load new files
  source config.sh
}

# For repos directly cloned
default_paths() {
  if [ -e functions/assets/user-variables.sh ] ; then
    rm functions/assets/user-variables.sh
  fi
  if [ -e .paths ] ; then
    echo "Removing current paths..." ; rm .paths
  fi

  LOCAL=$(_loc)
  GL=$LOCAL/
  printf "DEFAULT \$LOCAL:\n$LOCAL\n" > .paths
  printf "DEFAULT \$GL:\n$GL\n" >> .paths

  DSHELL=$GL'docs-shell'
  printf "DEFAULT \$DSHELL:\n$DSHELL\n" >> .paths
  DOC=$GL'gitlab-docs'
  printf "DEFAULT \$DOC:\n$DOC\n" >> .paths
  GITLAB=$GL'gitlab'
  printf "DEFAULT \$GITLAB:\n$GITLAB\n" >> .paths
  OMN=$GL'omnibus-gitlab'
  printf "DEFAULT \$OMN:\n$OMN\n" >> .paths
  RUN=$GL'gitlab-runner'
  printf "DEFAULT \$RUN:\n$RUN\n" >> .paths
  CHAR=$GL'charts'
  printf "DEFAULT \$CHAR:\n$CHAR\n" >> .paths
  symlink_gitlab
  symlinks
  tput setaf 3 ; echo `tput bold`"Check the result:"`tput sgr0` ; cat .paths
  user_variables
  echo "PORT='3000'" >> functions/assets/user-variables.sh
  # Load new files
  source config.sh
}
